class Employee{
  constructor({ id, rm_id, name, performance, last_vacation_date, salary }){
    this.id = id;
    this.rmid = rm_id;
    this.name = name;
    this.performance = performance;
    this.lastVacation = last_vacation_date;
    this.salary = salary;
  }
}

class ResourceManager extends Employee{
  constructor({ id, rm_id, name, performance, last_vacation_date, salary, pool_name }){
    super({ id, rm_id, name, performance, last_vacation_date, salary });

    this.poolname = pool_name;

    this.pool = [];
  }

  getAverageData(){
    const performances = {
      'low': 1,
      'average': 2,
      'top': 3
    };
    const performanceArray = ['low', 'average', 'top']
    const result = {
      performance: 0,
      salary: 0
    };
    this.pool.forEach(e => {
      if(e instanceof ResourceManager){
        result.performance += performances[e.getAverageData().performance];
        result.salary += e.getAverageData().salary;
      } else{
        result.performance += performances[e.performance];
        result.salary += e.salary;
      }
    })

    result.performance = performanceArray[Math.round(result.performance / this.pool.length) - 1];
    result.salary /= this.pool.length;

    return result;
  }

  getManagerById(id){
    if(this.id === parseInt(id)){
      return this;
    } else{
      let result = null;
      for(let i = 0; i < this.pool.length; ++i){
        if(this.pool[i] instanceof ResourceManager){
          result = this.pool[i].getManagerById(id);
        }
        if(result){
          return result;
        }
      }
    }
  }

  add(entity){
    if(this.id === entity.rmid){
      this.pool.push(entity);
    } else{
      this.pool.forEach(e => {
        if(e instanceof ResourceManager){
          e.add(entity);
        }
      })
    }
  }
}

class Filter{
  constructor(strategy){
    this.strategy = strategy;
  }

  execute(data){
    return this.strategy(data);
  }
}

const strategies = {
  default: (tree) => tree,
  lowPerformance: (tree) => {

    tree.pool = tree.pool
      .map(x => {
        if(x instanceof ResourceManager){
          return new Filter(strategies.lowSalary).execute(x);
        } else if(x.performance === 'low'){
          return x;
        }
        return null;
      })
      .filter(x => x !== null);

    return tree;
  },
  lowSalary: (tree) => {
    const avgSal = tree.getAverageData().salary;

    tree.pool = tree.pool
      .map(x => {
        if(x instanceof ResourceManager){
          return new Filter(strategies.lowSalary).execute(x);
        } else if(x.salary > avgSal){
          return x;
        }
        return null;
      })
      .filter(x => x !== null);

    return tree;
  }
}

document.addEventListener('DOMContentLoaded', async () => {
  let data;
  data = await fetch('mock.json')
    .then(data => data.json())

  let tree = buildTree(data);

  const tabs = document.querySelectorAll('.tab');
  document.querySelector('.tree-list').appendChild(renderEmployees(tree));

  for(let i = 0; i < tabs.length; ++i){
    tabs[i].addEventListener('click', async () => {
      let newdata = await fetch('mock.json')
        .then(data => data.json())
      tree = buildTree(newdata);
      document.querySelector('.tree-list').innerHTML = '';
      for(let j = 0; j < tabs.length; ++j){
        tabs[j].classList.remove('active');
      }
      tabs[i].classList.add('active');

      switch(tabs[i].id){
        case 'employees':
          document.querySelector('.tree-list').appendChild(renderEmployees(tree));
          break;
        case 'units':
          document.querySelector('.tree-list').appendChild(renderAverage(tree));
          break;
        default:
          renderFilter(tree);
          document.querySelector('.tree-list')
            .appendChild(renderEmployees(new Filter(strategies.default)
              .execute(tree)));
          break;
      }
    });
  }
})

function buildTree(data){
  const treeHead = new ResourceManager(...data.splice(0, 1));
  data.forEach(e => {
    if(e.pool_name){
      let temp = new ResourceManager({ ...e });
      treeHead.add(temp);
    } else{
      treeHead.add(new Employee({ ...e }));
    }
  });
  return treeHead;
}

function renderEmployees(data){
  const sitem = document.createElement('li');
  const sdesc = document.createElement('div');
  sdesc.classList.add('desc');
  sdesc.innerHTML = data.name;

  const sdetails = document.createElement('div');
  sdetails.innerHTML = 
  `Performance: ${data.performance}; Salary: ${data.salary}; ${data.last_vacation_date 
    ? `Last vacation date: ${data.last_vacation_date}` : ''}`;
  sdetails.classList.add('details', 'hidden');
  
  sdesc.addEventListener('click', () => {
    sdetails.classList.toggle('hidden');
  });

  sitem.appendChild(sdesc);
  sitem.classList.add('resource');
  sitem.setAttribute('data-id', data.id);
  sdesc.classList.add('rm');
  const wrapper = document.createElement('ul');
  data.pool.forEach(e => {
    if(e instanceof ResourceManager){
      wrapper.appendChild(renderEmployees(e));
    } else{
      const item = document.createElement('li');
      const desc = document.createElement('div');
      desc.classList.add('desc');
      desc.innerHTML = e.name;
  
      const details = document.createElement('div');
      details.innerHTML = `Performance: ${e.performance}; Salary: ${e.salary}; ${e.last_vacation_date 
        ? `Last vacation date: ${e.last_vacation_date}` : ''}`;
      details.classList.add('details', 'hidden');
      
      desc.addEventListener('click', () => {
        details.classList.toggle('hidden');
      });
  
      item.appendChild(desc);
      item.appendChild(details);
  
      wrapper.appendChild(item);
    }
  });
  sdetails.appendChild(wrapper);
  sitem.appendChild(sdetails);
  return sitem;
}

function renderAverage(data){
  const result = renderEmployees(data);
  const rmList = result.querySelectorAll('.resource');
  const _id = result.getAttribute('data-id');
  const _average = data.getManagerById(_id).getAverageData();
  const _avg = document.createElement('p');
  _avg.innerHTML = `Average: Performance: ${_average.performance}; Salary: ${_average.salary}.`;
  result.querySelector('.details').prepend(_avg);

  for(let i = 0; i < rmList.length; ++i){
    const id = rmList[i].getAttribute('data-id');

    const average = data.getManagerById(id).getAverageData();

    const avg = document.createElement('p');
    avg.innerHTML = `Average: Performance: ${average.performance}; Salary: ${average.salary}.`;
    rmList[i].querySelector('.details').prepend(avg);
  }

  return result;
}

function renderFilter(data){
  const filter = document.createElement('select');
  filter.classList.add('filter');

  const options = [
    {
      label: 'Select a filter',
      value: ''
    },
    {
      label: 'Low performance',
      value: 'performance'
    },
    {
      label: 'Low salary',
      value: 'salary'
    }
  ];

  options.forEach(op => {
    const opt= document.createElement('option');
    opt.setAttribute('value', op.value);
    opt.innerHTML = op.label;

    filter.appendChild(opt);
  });

  filter.addEventListener('change', () => {
    const value = document.querySelector('.filter').value;
    let strat;
    switch(value){
      case 'performance':
        strat = new Filter(strategies.lowPerformance);
        break;
      case 'salary':
        strat = new Filter(strategies.lowSalary);
        break;
      default:
        strat = new Filter(strategies.default);
    }

    let list = document.querySelector('.tree-list>.resource');
    if(list){
      list.remove();
      list = null;
    }
    document.querySelector('.tree-list').appendChild(renderEmployees(strat.execute(data)));
  });

  document.querySelector('.tree-list').prepend(filter);
}

function removeFilter(){
  let filter = document.querySelector('.filter');

  if(filter){
    filter.remove();
  }

  filter = null;
  return;
}