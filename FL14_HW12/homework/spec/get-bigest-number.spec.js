const {getBigestNumber} = require('../src/get-bigest-number');

describe('Checking getBigestNumber function', function() {
    it('should be at least one number', function() {
        expect(getBigestNumber(['1','2',3].toThrow(new Error('Wrong argument type')))
    });

    it('array should have at least two elements', function() {
        expect(getBigestNumber([1]).toThrow(new Error('Not enough arguments')))
    });

    it('array should have less than 10 arguments', function() {
        expect(getBigestNumber([1,2,3,4,5,6,7,8,9,10,11]).toThrow(new Error('Too many arguments')))
    });

    it('function should return the bigest number', function() {
        expect(getBigestNumber([1,2,3,4,5,6,7,8,9,10]).toEqual(10))
    });

});
