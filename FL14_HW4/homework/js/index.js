let suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades'];
let values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];

class Card {
    constructor(suit, value) {
        this.suit = suit;
        this.value = value;
    }
}

class Deck {
    constructor() {
        this.deck = [];
    }

    createDeck(suits, values) {
        for (let suit of suits) {
            for (let value of values) {
                this.deck.push(new Card(suit, value));
            }
        }
        return this.deck;
    }

    get count() {
        return this.deck.length;
    }

    shuffle() {
        let counter = this.deck.length, temp, i;

        while (counter) {
            i = Math.floor(Math.random() * counter--);
            temp = this.deck[counter];
            this.deck[counter] = this.deck[i];
            this.deck[i] = temp;
        }
        return this.deck;
    }

    deal() {
        let hand = [];
        while(hand.length < 7) {
            hand.push(this.deck.pop()); 
        }
        return hand;
    }
}

class Player {
    constructor(name, wins, deck) {
        this.name = name;
        this.wins = wins;
        this.deck = deck;
    }
}

class Employee {
    constructor(id, firstName, lastName, birthday, salary, position, department) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.salary = salary;
        this.position = position;
        this.department = department;
        Employee.EMPLOYEES.push(this);
    }

    static get EMPLOYEES() {
        if (!this.employee) {
            this.employee = [];
        }
        return this.employee;
    }

    get fullName() {
        return this.firstName + ' ' + this.lastName;
    }

    retire() {
        console.log('It was such a pleasure to work with you!');
        //  Employee.EMPLOYEES.indexOf(this);
        // Employee.EMPLOYEES.pop();
    }

    changeDepartment(newDepartment) {
        this.department = newDepartment;
    }

    changePosition(newPosition) {
        this.position = newPosition;
    }

    changeSalary(newSalary) {
        this.salary = newSalary;
    }
}

class Manager extends Employee {
    constructor(id, firstName, lastName, birthday, salary, position, department) {
        super(id, firstName, lastName, birthday, salary, position, department);
        this.position = 'manager';
    }
}

class BlueCollarWorker extends Employee { }

class HRManager extends Manager {
    constructor(id, firstName, lastName, birthday, salary, position, department) {
        super(id, firstName, lastName, birthday, salary, position, department);
        this.department = 'hr';
    }
}

class SalesManager extends Manager {
    constructor(id, firstName, lastName, birthday, salary, position, department) {
        super(id, firstName, lastName, birthday, salary, position, department);
        super.department = 'sales';
    }
}