const maxElement = array => Math.max.apply(null, array);

const copyArray = array => {
    const [...rest] = array;
    return [...rest];
};

const addUniqueId = obj => {
    const newObj = [obj];
    newObj['name'] = Symbol(Math.random());
    return newObj;
}

const oldObj = {
    name: 'Someone',
    details: {
        id: 1,
        age: 11,
        university: 'UNI'
    }
}

const regroupObject = obj => {
    const {name, details: { id, age, university } } = obj;
    const newObj = { university, user: {age, firstName: name, id}};
    return newObj;
}

const findUniqueElements = (...array) => {
    const set = new Set(...array);
    const arr = []
    for (let item of set) {
        arr.push(item);
    }
    return arr;
}

const hideNumber = phone => phone.padStart(6, '*');

const required = () => { 
    throw new Error('Missing property') 
};

const add = (x = required(), y = required()) => x + y;

const sortByAlphabet = (a, b) => {
    const nA = a.name;
    const nB = b.name;
    if (nA < nB) {
        return -1;
    } else if (nA > nB) {
        return 1;
    }
    return 0;
};

const url = 'https://jsonplaceholder.typicode.com/users';

const getData = url => fetch(url)
    .then(response => response.json())
    .catch(rejected => rejected.json()
        .then(error => {
            const e = new Error('Oops');
            e.data = error;
            throw e;
        })
    )

async function getDataAsync(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

getData(url)
    .then(data => console.log(data.sort(sortByAlphabet)))
    .catch(err => console.log(err));

getDataAsync(url)
    .then(data => console.log(data.sort(sortByAlphabet)))
    .catch(err => console.log(err));