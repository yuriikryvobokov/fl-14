$(function () {
	$('#list').lobiList({
		key: {
			Enter: 13
		}
	});
});


let tasks = JSON.parse(localStorage['tasks']);

const addTask = function (val) {
	$('#list').append('<li>' + val +
		`
	<a href='#' class='done-btn'>Done</a>
	<a href='#' class='cancel-btn'>Cancel Task</a>
	<a href='#' class='quickly-btn'>Quickly Task</a></li>
	`);
}

const saveLocalStorage = function () {
	if (localStorage['tasks']) {
		tasks = JSON.parse(localStorage['tasks']);
	} else {
		tasks = [];
	}
}

for (let i = 0; i < tasks.length; i++) {
	addTask(tasks[i]);
}

const addNewTask = function () {
	const val = $('#name').val();

	if (val.trim()) {
		tasks.push(val);
		localStorage['tasks'] = JSON.stringify(tasks);
		addTask(val);
		$('#name').val('').focus();
	} else {
		const e = new Error('Empty line');
		throw e;
	}
}

$('#add-btn').click(addNewTask);

$('#name').keyup(function (e) {
	if (e.keyCode === 13) {
		addNewTask();
	}
});

$('.done-btn').click('click', function () {
	$(this).parent('li').addClass('done');
	//saveLocalStorage();
});

$('.quickly-btn').click('click', function () {
	$(this).parent('li').addClass('quickly');
	//saveLocalStorage();
});

$('.cancel-btn').click('click', function () {
	$(this).parent('li').fadeOut();
	//saveLocalStorage();
});