//1. Write a function that accepts a person's date of birth as an input and returns the person's exact age today.
// For the sake of the task, let’s assume that the input is always a valid date object.
const getAge = birthDate => Math.floor((new Date() - new Date(birthDate).getTime()) / 3.15576e+10)
//I am using a year of 365.25 days (0.25 because of leap years) which are 3.15576e+10 milliseconds (365.25 * 24 * 60 * 60 * 1000) respectively.

//2. Write a function that accepts a date/timestamp and returns a textual representation of the corresponding 
//weekday (i.e. 'Monday', 'Tuesday', etc.). For the sake of the task, let’s assume that the input is always a valid date object or a timestamp.
сonst getWeekDay = date => {
    
}

getWeekDay(Date.now()); // "Thursday" (if today is the 22nd October)
getWeekDay(new Date(2020, 9, 22)); // "Thursday"