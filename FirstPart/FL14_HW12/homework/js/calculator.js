let altEval = expr => Function('return ' + expr)();

let calcValue = expr => {
    try {
        let result = altEval(expr.replace(/\s/g, ''));

        if (isNaN(result)) {
            throw new EvalError('Result of calculation is Not a Number');
        }
        if (result === undefined) {
            throw new EvalError('Result of calculation is undefined');
        }

        return result;

    } catch (e) {
        expr = prompt(`Error! ${e.message}\n\nTry again:`);
        calcValue(expr);
    }
}

let expression = prompt('Введите выражение: ');
calcValue(expression);
