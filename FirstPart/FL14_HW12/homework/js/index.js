function visitLink(path) {
	let count = localStorage.getItem(path);
	if (count === null) {
		count = 0;
	}
	count++;
	localStorage.setItem(path, count);
}

function viewResults() {
	let ul = "<ul>";
	for (let i = 0; i < localStorage.length; i++) {
		ul += "<li><a href=\"#\">" + localStorage[i] + "</a></li>";
	}
	ul += "</ul>";
}
