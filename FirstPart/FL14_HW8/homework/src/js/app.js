let arrayQuestions = JSON.parse(localStorage.getItem('questions'));
let question;
let doubleScore;

let startGame = document.querySelector('.btn__header_left');
let skipQuestion = document.querySelector('.btn__header_right');
let btnGroup = document.querySelectorAll('.btn__group');
let divGroup = document.querySelectorAll('.btn__hidden');
let mainQuestion = document.querySelector('.main__question');
let scorePrize = document.querySelector('.scorePrize');
let currentPrize = document.querySelector('.currentPrize');
let divShow = document.querySelector('.div__show');

const SETTINGS = {
    MAX_PRIZE: 1000000,
    FIRST_POINT: 100,
    DOUBLE_RESULT: 2,
    SCORE: 0
}

function getQuestion() {
    let item = Math.floor(Math.random() * arrayQuestions.length | 0);
    return arrayQuestions.splice(item, 1)[0];
}

function fillButton() {
    question = getQuestion();
    mainQuestion.innerHTML = question.question;

    for (let i = 0; i < btnGroup.length; i++) {
        btnGroup[i].innerHTML = question.content[i];
    }
}

function displayButtonBlock() {
    for (let element of divGroup) {
        element.style = 'display: block';
    }
}

startGame.addEventListener('click', function () {
    doubleScore = SETTINGS.FIRST_POINT;
    divShow.innerHTML = '';
    displayButtonBlock();
    fillButton();
    scorePrize.innerHTML = SETTINGS.SCORE;
    currentPrize.innerHTML = doubleScore;
})

skipQuestion.addEventListener('click', function () {
    fillButton();
    skipQuestion.style = 'display: none';
})


for (let i = 0; i < btnGroup.length; i++) {
    btnGroup[i].addEventListener('click', function () {

        if (i === question.correct) {
            SETTINGS.SCORE += doubleScore;
            doubleScore *= SETTINGS.DOUBLE_RESULT;

            if (SETTINGS.SCORE >= SETTINGS.MAX_PRIZE) {
                displayButtonBlock();
                divShow.style = 'display: block';
                divShow.innerHTML = `Congratulations! Your won: ${SETTINGS.MAX_PRIZE}`;
                currentPrize.innerHTML = SETTINGS.SCORE;
            } else {
                fillButton();
                scorePrize.innerHTML = SETTINGS.SCORE;
                currentPrize.innerHTML = doubleScore;
            }
        } else {
            for (let element of divGroup) {
                element.style = 'display: none';
            }

            divShow.style = 'display: block';
            divShow.innerHTML = `Game over. Your prize is: ${SETTINGS.SCORE}`;
            currentPrize.innerHTML = SETTINGS.SCORE;
        }
    });
}


