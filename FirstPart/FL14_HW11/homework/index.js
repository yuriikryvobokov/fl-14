const isEquals = (valueOne, valueTwo) => valueOne === valueTwo;

const numberToString = (...arr) => {
    for (let i = 0; i < arr.length; i++) {
        arr[i] += '';
    }
    return arr;
}

const storeNames = (...arr) => arr;

const getDivision = (valueOne, valueTwo) => valueOne > valueTwo ? valueOne / valueTwo : valueTwo / valueOne;

const negativeCount = arr => {
    let counter = 0;

    for (const element of arr) {
        element < 0 ? counter++ : 0;
    }
    return counter;
}

const letterCount = (valueOne, valueTwo) => {
    let counter = 0;

    for (let i = 0; i < valueOne.length; i++) {
        if (valueOne[i].includes(valueTwo)) {
            counter++;
        }
    }
    return counter;
}

const countPoints = arr => {
    const count = arr.reduce(function (accamulator, currentValue) {
        const points = {
            more: 3,
            less: 0,
            equal: 1
        }

        const splitStr = currentValue.split(':');
        const resultOne = parseInt(splitStr[0]);
        const resultTwo = parseInt(splitStr[1]);

        if (resultOne > resultTwo) {
            accamulator += points.more;
            console.log(accamulator)
        } else if (resultOne < resultTwo) {
            accamulator += points.less;
        } else {
            accamulator += points.equal;
        }

        return accamulator;
    }, 0)

    return count;
};