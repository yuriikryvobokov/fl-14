const convert = (...arr) => {
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'string') {
            parseInt(arr[i]);
        } else {
            arr[i] += '';
        }
    }
}

//convert('1', 2, 3, '4') // [1, '2', '3', 4]

const executeforEach = (arr, obj) => {
    for (let numbers of arr) {
        obj(numbers);
    }
}

/*executeforEach([1, 2, 3, 5], function (el) {
    console.log(el * 2);
}); */

const mapArray = (arr, obj) => {
    let mapArr = [];
    executeforEach(arr, function (el) {
        mapArr.push(obj(el));
    });
    return mapArr;
}

//console.log(mapArray([2, '5', 8], function (el) {if (typeof el === "string") {el = + el;}return el + 3;}));

const filterArray = (arr, obj) => {
    let filteredArray = [];
    executeforEach(arr, function (el) {
        if (obj(el)) {
            filteredArray.push(el);
        }
    })
    return filteredArray;
}

/*console.log(filterArray([2, 5, 8], function (el) {return el % 2 === 0;}));*/ // returns [2, 8]

const getValuePosition = (arr, value) => {
    let i = arr.length;
    while (i--) {
        if (arr[i] === value) {
            return i + 1;
        }
    }
    return false;
}

/********************************************************* */
//console.log(getValuePosition([2, 5, 8], 8)); // returns 3 
//console.log(getValuePosition([12, 4, 6], 1)); // returns false

const flipOver = arr => {
    let join = ' ';
    for (let i = arr.length - 1; i >= 0; i--) {
        join += arr[i];
    }
    return join;
}

//console.log(flipOver('hey world'));

const makeListFromRange = arr => {
    let tempArray = [];
    for (let i = arr[0]; i < arr[1] - arr[0]; i++) {
        if (arr[i] <= arr[1]) {
            tempArray.push(arr[i]);
        }
    }
    return tempArray;
}

//console.log(makeListFromRange([2, 7])); // [2, 3, 4, 5, 6, 7]

const fruits = [
    { name: 'apple', weight: 0.5 },
    { name: 'pineapple', weight: 2 }
];

const getArrayOfKeys = (fruits, key) => {
    const arrOfKeys = [];
    executeforEach(fruits, (obj) => {
        arrOfKeys.push(obj[key]);
    })
    return arrOfKeys;
}

//console.log(getArrayOfKeys(fruits, 'name')); // returns [‘apple’, ‘pineapple’]

const basket = [
    { name: 'Bread', weight: 0.3 },
    { name: 'Coca-Cola', weight: 0.5 },
    { name: 'Watermelon', weight: 8 }
];

///

//getTotalWeight(basket) // returns 8.8

function getPastDay(date, days) {
    let copy = new Date(date);
    copy.setDate(date.getDate() - days);
    let dateNumber = copy.getDate();

    return dateNumber;
}

//const date = new Date(2020, 0, 2);
//console.log(getPastDay(date, 1)); // 1, (1 Jan 2020)
//console.log(getPastDay(date, 2)); // 31, (31 Dec 2019)
//console.log(getPastDay(date, 365)); // 2, (2 Jan 2019)

function formatDate(date) {

    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    let hours = date.getHours();
    let minutes = date.getMinutes();

    return year + '/' + month + '/' + day + ' ' + hours + ':' + minutes;
    //return date.toLocaleString();
}

//console.log(formatDate(new Date('6/15/2019 09:15:00'))) // "2019/06/15 09:15"
//console.log(formatDate(new Date())) // "2020/04/07 12:56" // gets current local time
