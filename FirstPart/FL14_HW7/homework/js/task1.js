'use strict';
const maxPercentage = 100;


function checkBatteries() {
    let amountBatteries = parseInt(prompt('Enter the amount of batteries: ', 0));

    if (amountBatteries > 0) {

        let percentageBatteries = + prompt('Enter the percentage of defective batteries: ', 0);

        if (percentageBatteries < 0 || percentageBatteries > maxPercentage) {
            alert('Invalid input data');
        } else {
            let calcBatteries = amountBatteries * percentageBatteries / maxPercentage;
            let workingBatteries = amountBatteries - calcBatteries;
            alert(`Amount of batteries: ${amountBatteries}` + '\n' +
                `Defective rate: ${percentageBatteries}` + '\n' +
                `Amount of defective batteries: ${calcBatteries}` + '\n' +
                `Amount of working batteries: ${workingBatteries}`);
        }
    } else {
        alert('Invalid input data');
    }
}

checkBatteries();