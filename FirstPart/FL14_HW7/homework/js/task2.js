'use strict';

const oddNumber = 2;

function checkWord() {
    let strCharacter = prompt('Enter a word: ');
    let middleLen;

    if (strCharacter && strCharacter !== '' && strCharacter.trim() !== '') {

        if (strCharacter.length % oddNumber === 0) {
            middleLen = strCharacter.length / oddNumber - 1;
            if (strCharacter[middleLen] === strCharacter[middleLen + 1]) {
                alert('Middle characters are the same');
            } else {
                alert(`Middle characters is: "${strCharacter[middleLen]}${strCharacter[middleLen + 1]}"`);
            }
        } else {
            middleLen = Math.floor(strCharacter.length / oddNumber);
            alert(` Middle character is "${strCharacter[middleLen]}"`);
        }
    } else {
        alert('Invalid value');
    }
}

checkWord();
