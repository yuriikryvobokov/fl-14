const requestURL = 'https://jsonplaceholder.typicode.com/users';
const spinner = document.getElementById('spinner');

const body = {
    name: 'Yurii',
    age: 22
}

function showSpinner() {
    spinner.className = 'show';
    setTimeout(() => {
        spinner.className = spinner.className.replace('show', '');
    }, 5000);
}

function loadData() {
    spinner.removeAttribute('hidden');
    showSpinner();
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(data => {
            spinner.setAttribute('hidden', '');
            console.log(data)
        })
        .catch(rejected => {
            return rejected.json().then(error => {
                const e = new Error('Oops');
                e.data = error;
                throw e;
            })
        })
}

function getRequest(method, url) {
    return fetch(url)
        .then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            }
        })
        .catch(rejected => {
            return rejected.json().then(error => {
                const e = new Error('Oops');
                e.data = error;
                throw e;
            })
        })
}

function postRequest(method, url, body = null) {

    const headers = {
        'Content-Type': 'application/json'
    }

    return fetch(url, {
        method: method,
        body: JSON.stringify(body),
        headers: headers
    })
    .then(response => {
        if (response.ok) {
            return response.json();
        }
        return response.json().then(error => {
            const e = new Error('Oops');
            e.data = error;
            throw e;
        })
    })
}

function putRequest(method, url) {

    const headers = {
        'Content-Type': 'application/json'
    }

    return fetch(url, {
        method: method,
        body: JSON.stringify({
            id: 1,
            name: 'Yurii'
        }),
        headers: headers
    }).then(response => {
        if (response.ok) {
            return response.json();
        }
        return response.json().then(error => {
            const e = new Error('Oops');
            e.data = error;
            throw e;
        })
    })
}

function deleteRequest(method, url) {

    return fetch(url, {
        method: method
    }).then(response => {
        if (response.ok) {
            return response.json()
        }
        return response.json().then(error => {
            const e = new Error('Oops');
            e.data = error;
            throw e;
        })
    })
}

getRequest('GET', requestURL)
    .then(data => console.log(data))
    .catch(err => console.log(err));

postRequest('POST', requestURL, body)
    .then(data => console.log(data))
    .catch(err => console.log(err));

putRequest('PUT', 'https://jsonplaceholder.typicode.com/users/1')
    .then(data => console.log(data))
    .catch(err => console.log(err));

deleteRequest('DELETE', 'https://jsonplaceholder.typicode.com/users/1')
    .then(data => console.log(data))
    .catch(err => console.log(err));